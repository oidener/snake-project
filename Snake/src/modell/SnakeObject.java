package modell;

import java.awt.EventQueue;
import javax.swing.JFrame;

import komponent.Client;
import komponent.SnakeApp;

public class SnakeObject extends JFrame {
	
	public SnakeObject() {
		initUI();
		// TODO Auto-generated constructor stub
	}
     	    
    public void initUI() {
        
        add(new SnakeApp());
               
        setResizable(false);
        pack();
        
        setTitle("Snake");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
    	Client client = new Client("localhost", 8000);
		client.sendMessage("Guten Tag, Frau Server! Wie geht es Ihnen?");

        EventQueue.invokeLater(() -> {
            JFrame ex = new SnakeObject();
            ex.setVisible(true);
        });
    }            
}
