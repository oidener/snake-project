package komponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import javax.swing.Timer;

public class SnakePresenter implements ActionListener {

	public final int ALL_DOTS = 900;
	final int x[] = new int[ALL_DOTS];
	final int y[] = new int[ALL_DOTS];

	private final int RAND_POS = 29;
	public final int DOT_SIZE = 10;
	public int dots;
	public int Score = 0;

	public int apple_x;
	public int apple_y;
	public boolean inGame = true;

	private final int DELAY = 100;
	private SnakeApp snakeapp;
	private Highscore highscore = new Highscore(0);
	
	public SnakePresenter(SnakeApp snakeapp) {
		this.snakeapp = snakeapp;
		this.snakeapp.setFocusable(true);
	}
			
	public void game() {

		Score = 0;
		dots = 3;
		for (int z = 0; z < dots; z++) {
			x[z] = 150 - z * 10;
			y[z] = 150;
		}

		standortApfel();
		if (snakeapp.timer == null) snakeapp.timer = new Timer(DELAY, this);
		snakeapp.timer.start();
	}

	private void ueberpruefeApfel() {

		if ((x[0] == apple_x) && (y[0] == apple_y)) {

			dots++;
			Score++;
			standortApfel();
		}
	}

	private void standortApfel() {

		int r = (int) (Math.random() * RAND_POS);
		apple_x = ((r * DOT_SIZE));

		r = (int) (Math.random() * RAND_POS);
		apple_y = ((r * DOT_SIZE));
	}

	private void checkCollision() {

		for (int z = dots; z > 0; z--) {

			if ((z > 3) && (x[0] == x[z]) && (y[0] == y[z])) {
				inGame = false;
			}
		}

		if (y[0] >= snakeapp.B_HEIGHT) {
			inGame = false;
		}

		if (y[0] < 0) {
			inGame = false;
		}

		if (x[0] >= snakeapp.B_WIDTH) {
			inGame = false;
		}

		if (x[0] < 0) {
			inGame = false;
		}

		if (!inGame) {
			snakeapp.timer.stop();
			Highscore highscore = readHighscore("highscore.bin");
			highscore.updateHighscore(Score);
			writeHighscore(highscore, "highscore.bin");
		}
	}
	
	public void move() {

		for (int z = dots; z > 0; z--) {
			x[z] = x[(z - 1)];
			y[z] = y[(z - 1)];
		}

		if (snakeapp.leftDirection) {
			x[0] -= DOT_SIZE;
		}

		if (snakeapp.rightDirection) {
			x[0] += DOT_SIZE;
		}

		if (snakeapp.upDirection) {
			y[0] -= DOT_SIZE;
		}

		if (snakeapp.downDirection) {
			y[0] += DOT_SIZE;
		}
	}
			
	public void writeHighscore(Highscore highscore, String dateiname) {

		//Dekorieren des FileOutputStreams mit einem DataOutputStream
		try (ObjectOutputStream fos = new ObjectOutputStream(new FileOutputStream(dateiname))){
			 
			fos.writeObject(highscore);
						
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	public Highscore readHighscore(String dateiname) {

		File f = new File(dateiname);

		if(!f.exists()) {
			return highscore;
		}
		
		try (ObjectInputStream fis = new ObjectInputStream(new FileInputStream(dateiname))){			
			highscore = (Highscore) fis.readObject();		
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return highscore;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.snakeapp.requestFocus();
				
		if (inGame) {
			ueberpruefeApfel();
			checkCollision();
			move();
		}
		snakeapp.repaint();
	}

	public Highscore getHighscore() {
		return highscore;
	}

	public void setHighscore(Highscore highscore) {
		this.highscore = highscore;
	}
}
