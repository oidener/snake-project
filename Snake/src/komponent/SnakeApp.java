package komponent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class SnakeApp extends JPanel {

		public final int B_WIDTH = 400;
	    public final int B_HEIGHT = 400;
	    public Timer timer;
	    private JTextField txtSnake;
	    	    
	    private SnakePresenter snakepresenter;
	    private SnakeImages snakeimages;
	    
	    public boolean leftDirection = false;
	    public boolean rightDirection = true;
	    public boolean upDirection = false;
	    public boolean downDirection = false;
	    
	/**
	 * Create the panel.
	 */
	public SnakeApp() {

		initialize();
	}
	
	 private void initialize() {

	        addKeyListener(new TAdapter());
	        setBackground(Color.black);
	        setFocusable(true);

	        setPreferredSize(new Dimension(600, 435));
	        snakepresenter = new SnakePresenter(this);
	        snakepresenter.game();
	        snakeimages = new SnakeImages(this);
	        snakeimages.loadImages();
	       
	        JPanel panel = new JPanel();
	        panel.setBorder(new LineBorder(Color.WHITE));
	        panel.setOpaque(false);
	        panel.setPreferredSize(new Dimension(400, 400));
	                
	        JToolBar toolBar = new JToolBar();
	        toolBar.setBorderPainted(false);
	        toolBar.setOpaque(false);
	        
	        JPanel panel_score = new JPanel();
	        panel_score.setBorder(new LineBorder(Color.WHITE, 2));
	        panel_score.setOpaque(false);
	       
	        txtSnake = new JTextField();
	        txtSnake.setHorizontalAlignment(SwingConstants.CENTER);
	        txtSnake.setOpaque(false);
	        txtSnake.setForeground(Color.WHITE);
	        txtSnake.setText("SNAKE");
	        txtSnake.setFont(new Font("Arial", Font.BOLD, 22));
	        txtSnake.setBorder(new LineBorder(Color.WHITE, 2, true));
	        txtSnake.setColumns(10);
	        GroupLayout groupLayout = new GroupLayout(this);
	        groupLayout.setHorizontalGroup(
	        	groupLayout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(groupLayout.createSequentialGroup()
	        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
	        				.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
	        				.addGroup(groupLayout.createSequentialGroup()
	        					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 412, GroupLayout.PREFERRED_SIZE)
	        					.addPreferredGap(ComponentPlacement.UNRELATED)
	        					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
	        						.addComponent(panel_score, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
	        						.addComponent(txtSnake, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))))
	        			.addContainerGap())
	        );
	        groupLayout.setVerticalGroup(
	        	groupLayout.createParallelGroup(Alignment.LEADING)
	        		.addGroup(groupLayout.createSequentialGroup()
	        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
	        				.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	        				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
	        					.addGap(19)
	        					.addComponent(panel_score, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
	        					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	        					.addComponent(txtSnake, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
	        			.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	        			.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	        			.addContainerGap())
	        );

	        JButton btnNewButton = new JButton("Start");
	        toolBar.add(btnNewButton);
	        btnNewButton.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		timer.start();
	        	}
	        });
	        
	        JButton btnNewButton_1 = new JButton("Restart");
	        toolBar.add(btnNewButton_1);	        
	        btnNewButton_1.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		snakepresenter.inGame = true;
	        		snakepresenter.game();
	        	}
	        });
	        
	        JButton btnNewButton_2 = new JButton("Pause");
	        toolBar.add(btnNewButton_2);
	        btnNewButton_2.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		timer.stop();
	        		//Pause();
	        		    }
	        });	
	        
	        JButton btnNewButton_3 = new JButton("Exit");
	        toolBar.add(btnNewButton_3);
	        setLayout(groupLayout);
	        btnNewButton_3.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		System.exit(0);
	        	}
	        });
	    }
	 
	 @Override
	    public void paintComponent(Graphics g) {
	        super.paintComponent(g);

	        if (snakepresenter.inGame) {

	            g.drawImage(snakeimages.apfel, snakepresenter.apple_x, snakepresenter.apple_y, this);

	            for (int z = 0; z < snakepresenter.dots; z++) {
	                if (z == 0) {
	                    g.drawImage(snakeimages.kopf, snakepresenter.x[z],snakepresenter.y[z], this);
	                } else {
	                    g.drawImage(snakeimages.body, snakepresenter.x[z], snakepresenter.y[z], this);
	                }

	                g.setColor(Color.white);
	                g.setFont(new Font("arial", Font.PLAIN, 20));
	                g.drawString("Scores: " + snakepresenter.Score, 465, 45);
	        }
	            
	            Toolkit.getDefaultToolkit().sync();

	        } else {

	            gameOver(g);
	        }        
	    }
	    
	  private void gameOver(Graphics g) {    
		  
	        String msg = "BETTER LUCK NEXT TIME (Highscore: " + snakepresenter.getHighscore().getHighscore() + ")";
	        Font small = new Font("Times New Roman", Font.BOLD, 16);
	        FontMetrics metr = getFontMetrics(small);
	        
	        g.setColor(Color.white);
	        g.setFont(small);
	        g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);
	        
	        g.setColor(Color.white);
            g.setFont(new Font("arial", Font.PLAIN, 20));
            g.drawString("Scores: " + snakepresenter.Score, 465, 45);
	  }
	 
	 public class TAdapter extends KeyAdapter {

	        @Override
	        public void keyPressed(KeyEvent e) {

	            int key = e.getKeyCode();

	            if ((key == KeyEvent.VK_LEFT) && (!rightDirection)) {
	                leftDirection = true;
	                upDirection = false;
	                downDirection = false;
	            }

	            if ((key == KeyEvent.VK_RIGHT) && (!leftDirection)) {
	                rightDirection = true;
	                upDirection = false;
	                downDirection = false;
	            }

	            if ((key == KeyEvent.VK_UP) && (!downDirection)) {
	                upDirection = true;
	                rightDirection = false;
	                leftDirection = false;
	            }

	            if ((key == KeyEvent.VK_DOWN) && (!upDirection)) {
	                downDirection = true;
	                rightDirection = false;
	                leftDirection = false;
	            }
	        }
	    }

	public int getB_WIDTH() {
		return B_WIDTH;
	}

	public int getB_HEIGHT() {
		return B_HEIGHT;
	}

	public boolean isLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(boolean leftDirection) {
		this.leftDirection = leftDirection;
	}

	public boolean isRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(boolean rightDirection) {
		this.rightDirection = rightDirection;
	}

	public boolean isUpDirection() {
		return upDirection;
	}

	public void setUpDirection(boolean upDirection) {
		this.upDirection = upDirection;
	}

	public boolean isDownDirection() {
		return downDirection;
	}

	public void setDownDirection(boolean downDirection) {
		this.downDirection = downDirection;
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}
}
