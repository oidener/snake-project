package komponent;

import java.awt.Image;
import javax.swing.ImageIcon;

public class SnakeImages {
 
	public Image body;
    public Image apfel;
    public Image kopf;
    
    private SnakeApp snakeapp;
    
    public SnakeImages(SnakeApp snakeapp) {
    	this.snakeapp = snakeapp;
    }
    
    public void loadImages() {

        ImageIcon dot = new ImageIcon("src/resources/dot.png");
        body = dot.getImage();

        ImageIcon apple = new ImageIcon("src/resources/apple.png");
        apfel = apple.getImage();

        ImageIcon head = new ImageIcon("src/resources/head.png");
        kopf = head.getImage();
    }
}
