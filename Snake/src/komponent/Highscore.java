package komponent;

import java.io.Serializable;

public class Highscore implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int highscore;
//	private SnakePresenter snakepresenter;
	
	public Highscore(int highscore) {
		this.highscore = highscore;
		
	}
	
	public void updateHighscore(int score) {
		
		if(score > highscore) {
			highscore = score;
		}
		
	}
	
	public int getHighscore() {
		return highscore;
	}

	public void setHighscore(int highscore) {
		this.highscore = highscore;
	}
}
