package server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

	private int port;
	
	public static void main(String[] args) {
		Server server = new Server(8000);
		server.startListening();
	}
		
	public Server(int port) {
		this.port = port;
	}
	
	public void startListening() {
		
		System.out.println("[Server] Server starten...");
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				while(true) {
				
				try {
					ServerSocket serverSocket = new ServerSocket(port);
					System.out.println("[Server] Warten auf Verbindung.....");
					Socket remoteClientSocket = serverSocket.accept();
					System.out.println("[Server] Client verbunden: " + remoteClientSocket.getRemoteSocketAddress());
					
					Scanner s = new Scanner(new BufferedReader(new InputStreamReader(remoteClientSocket.getInputStream())));
					if(s.hasNextLine()) {
						System.out.println("[Server] Message from client: " + s.nextLine());
					}
					
					PrintWriter pw = new PrintWriter(new OutputStreamWriter(remoteClientSocket.getOutputStream()));
					pw.println("Mir geht es gut, danke der Nachfrage!");
					pw.flush();
					
					// Verbindung schlie�en
					s.close();
					pw.close();
					remoteClientSocket.close();
					serverSocket.close();
							
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}).start();
		
	}
	
}
